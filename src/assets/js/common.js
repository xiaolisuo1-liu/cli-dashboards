
const apiurl = "https://api-dev.aixunhuan.com.cn/api/v1.0"
const imgurl = "http://180.153.21.127/"

function ajaxzh(url, data, method, successfn, successfndata) {
	// if (localStorage.getItem('token')) {
	// 	data.token = localStorage.getItem('token')
	// }
	if (method == "POST") {
		axios.post(apiurl + url, data, {
				headers: {
					'Authorization': localStorage.getItem('token')
				},
			})
			.then(function(response) {
				successfn(response, successfndata);
				if(response.data.msg){
					if(response.data.code==0){
						example1.$message({
							message: response.data.msg,
							type: 'success'
						});
					}else{
						example1.$message({
							message: response.data.msg,
							type: 'error'
						});
					}
					
				}else{
					example1.$message({
						message: "操作成功",
						type: 'success'
					});
				}
				
			})
			.catch(function(error) {
				
			});
	} else if (method == "GET") {
		const apiurl = "https://api-dev.aixunhuan.com.cn/api/v1.0"
		const imgurl = "http://180.153.21.127/"
		var getUrl = apiurl + url;
		    $.ajax({
		        url:getUrl,
		        dataType:'json',
		        type:'get',
		        beforeSend: function (XMLHttpRequest) {
		            XMLHttpRequest.setRequestHeader("Content-Type", "application/json");
		            XMLHttpRequest.setRequestHeader("Client-key", "DA$H!BO^AR%D_V2");
		            XMLHttpRequest.setRequestHeader("languageId", 1);
		            XMLHttpRequest.setRequestHeader("x-auth","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZWYwYTgwZjA2MGUwNjdhYjEyNjljYTgiLCJfaWQiOiI1ZWYwYTgwZjA2MGUwNjdhYjEyNjljYTgiLCJ1c2VyX3R5cGUiOjQsImFjY2Vzc19jb2RlcyI6WyIzMDAwIiwiMjAwMCIsIjQwMDAiLCI1MDAwIiwiNzAwMCIsIjgwMDAiLCI5MDAwIiwiMTEwMDAiLCIxMTAwMSIsIjExMDAyIiwiMTEwMDMiLCI1MDAxIiwiNTAwMiIsIjExMDA0IiwiMTAwMCIsIjEwMDAwIiwiMTIwMDAiLCIxMzAwOSIsIjI0MDAwIiwiMjQwMDEiLCIyNDAwOSIsIjE4MDAyIiwiMTgwMDAiLCIxODAwMSIsIjIyMDAwIiwiMTAwMDEiLCIyMDAwOSIsIjIxMDAwIiwiMjAwMDEiLCIxMzAwMSIsIjQwMDkiLCIxODAwMyIsIjMwMDAwIiwiNjAwMDAiLCI1MDA5IiwiMjIyMjIyIiwiNjAwMCIsIjI0MDA4IiwiMzMwMDAiLCIzMjAwMCIsIjE4MDA3IiwiMTgwMDYiLCIxOTAwMCIsIjUwMDQiLCIxNDAwMSIsIjE0MDAyIiwiMTQwMDQiLCIxNTAwMSIsIjkwMDIiLCIxNTAwMiIsIjE1MDA0IiwiMTkwMDkxIiwiMTgwMDMiLCIxODAwNCIsIjE4MDA1IiwiOTAyMTAiLCI5MzE5MzEyNSIsIjkwMjE4IiwiOTAyMTkiXSwiZXhwaXJlZF9hdCI6IjE2MDQ4NDg1NTg3ODYyNTkyMDAwMDAwIiwiYWNjZXNzIjoiYXV0aCIsIndlY2hhdF9vcGVuaWQiOm51bGwsImlhdCI6MTYwNDg0ODU1OCwiZXhwIjoxNjA0OTM0OTU4fQ.yM8iCNliuEx8a2PS-oHqvjVn2T1pX1dBGfUg91GLqnM");
		        },
		        success:function(data){
					return data
		        }})
	}else if(method == "PUT"){
		axios.put(apiurl + url, data, {
				headers: {
					'Authorization': localStorage.getItem('token')
				},
			})
			.then(function(response) {
				successfn(response, successfn);
				example1.$message({
					message: response.data.msg,
					type: 'success'
				});
			})
			.catch(function(error) {
				
			});
	}else if(method == "DELETE"){
		axios.delete(apiurl + url, {
		    params:data,
		    headers: {
		    	'Authorization': localStorage.getItem('token')
		    },
		})
		.then(function(response) {
			successfn(response, successfndata);
		})
		.catch(function(error) {
			
		});
	}
}
