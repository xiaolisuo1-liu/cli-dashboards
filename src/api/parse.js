import $ from "../assets/js/jquery";
var baseUrl = "https://api.aixunhuan.com.cn/api/v1.0";

export function gains(url) {
  var datas = null;
  // console.log(baseUrl + url);s
  $.ajax({
    url: baseUrl + url,
    dataType: "json",
    contentType: "application/x-www-form-urlencoded",
    type: "get",
    async: false,
    beforeSend: function(XMLHttpRequest) {
      var query_url = window.location.href;
      let url = new URL(query_url);
      let searchParams = new URLSearchParams(url.search);
      var token = searchParams.get("cookie");
      //token
      XMLHttpRequest.setRequestHeader("Content-Type", "application/json");
      XMLHttpRequest.setRequestHeader("Client-key", "DA$H!BO^AR%D_V2");
      XMLHttpRequest.setRequestHeader("languageId", 1);
      XMLHttpRequest.setRequestHeader("x-auth", token);
    },
    success: function(data) {
      datas = data;
    }
  });
  return datas;
}

//调用api的时间
export function timeIs() {
  return 5000;
}

//导出根据不同的账号显示不同的logo
export function logoShow() {
  //获取white_label_id
  var query_url = window.location.href;
  let url = new URL(query_url);
  let searchParams = new URLSearchParams(url.search);
  var white_label = searchParams.get("white_label_id");
  // console.log(white_label);

  let obj = {
    // yaohai: {
    //   title: "瑶海区城管局垃圾分类大数据驾驶舱",
    //   title2: "瑶海区城管局督导",
    //   iconUrl: ""
    // }
    yaohai: {
      title: "爱凤环垃圾分类大数据驾驶舱",
      title2: "爱凤环督导",
      iconUrl: ""
    }
  };
  return obj;
}
