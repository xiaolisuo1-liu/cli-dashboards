//配置路由
import Vue from "vue";
import VueRouter from "vue-router";
// 使用插件
Vue.use(VueRouter);

//引入路由组件
import Home from "../pages/Home";
import Search1 from "../pages/Search1";
import Search2 from "../pages/Search2";
import Search3 from "../pages/Search3";
import Login from "../pages/Login";
import Bottom from "../pages/Bottom";
import Bottomt from "../pages/Bottomt";

export default new VueRouter({
  routes: [
    {
      path: "/home",
      component: Home
    },
    {
      path: "/search1",
      component: Search1
    },
    {
      path: "/search2",
      component: Search2
    },
    {
      path: "/search3",
      component: Search3
    },
    {
      path: "/login",
      component: Login
    },
    {
      path: "/Bottom",
      component: Bottom
    },
    {
      path: "/Bottomt",
      component: Bottomt
    },

    // 重定向， 让项目运行起来的时候访问首页
    {
      path: "*",
      redirect: "/home"
    }
  ]
});
